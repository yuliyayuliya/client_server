package com.julia.task;

import org.json.simple.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class ClientUi extends javax.swing.JFrame implements ConnectionListener {

    public ClientUi() {
        initComponents();
        setState(false);
    }

    void initComponents() {

        this.setSize(700, 700);
        this.setLocationRelativeTo(this);
        this.setVisible(true);
        JPanel panel = new JPanel();
        hostText = new JTextField(40);
        hostText.setText("127.0.0.1");
        connectButton = new JButton("connect");
        connectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                connectActionPerformed();
            }
        });
        textArea = new JTextArea(25, 45);
        commandText = new JTextField(40);
        sendButton = new JButton(" send ");
        sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendActionPerformed();
            }
        });
        panel.add(hostText);
        panel.add(connectButton);
        panel.add(textArea);
        panel.add(commandText);
        panel.add(sendButton);
        this.add(panel);
    }

    private void connectActionPerformed() {

        try {
            String host = hostText.getText().trim();
            Socket socket = new Socket(InetAddress.getByName(host), Connection.PORT);
            this.connection = new ConnectionImpl(socket, this);
            setState(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendActionPerformed() {
        inputMessage();
    }


    public void connectionCreated(Connection connection) {
        System.out.println("Client connection was created ");
        setState(true);
        this.connection = connection;
    }


    public void connectionClosed(Connection connection) {
        System.out.println("Client connection was closed ");
    }


    public void connectionException(Connection connection, Exception ex) {
        ex.printStackTrace();
    }


    public void recievedContent(Message message, Connection connection) {
        textArea.append("\n");
        textArea.append(message.toString());

    }

    private void setState(boolean isConnected) {
        textArea.setEnabled(isConnected);
        commandText.setEnabled(isConnected);
        sendButton.setEnabled(isConnected);
        connectButton.setEnabled(!isConnected);
        hostText.setEnabled(!isConnected);
        textArea.setText("\n" + "Examples of entering commands:" +
                "\n" + "\n" + "insert word description"
                + "\n" + "delete word"
                + "\n" + "select word"
                + "\n" + "update word new word" + "\n");
    }

    private void inputMessage() {
        ArrayList<String> arrayList = new ArrayList<String>();
        String string = commandText.getText();
        for (String s : string.split(" ")) {
            arrayList.add(s);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("command", arrayList.get(0));
        jsonObject.put("param", arrayList.get(1));
        if (jsonObject.get("command").equals("insert") || jsonObject.get("command").equals("update"))
            jsonObject.put("param2", arrayList.get(2));
        Message message = new MessageImpl(jsonObject.toJSONString());
        connection.send(message);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                ClientUi clientUi = new ClientUi();
                clientUi.setVisible(true);
                clientUi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }

    private JButton connectButton;
    private JButton sendButton;
    private JTextArea textArea;
    private JTextField hostText;
    private JTextField commandText;
    private Connection connection;

}
