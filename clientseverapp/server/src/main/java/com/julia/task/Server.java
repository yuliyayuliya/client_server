package com.julia.task;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.ServerSocket;

import java.net.Socket;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

public class Server implements ConnectionListener {

    public Server() {
        try {
            serverSocket = new ServerSocket(Connection.PORT);
            connections = new LinkedHashSet();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void startServer() {
        System.out.println("Server started");
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                connections.add(new ConnectionImpl(socket, this));
                jdbc.createDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    public void connectionCreated(Connection connection) {
        System.out.println("Connection was added ");

    }

    public void connectionClosed(Connection connection) {
        try {
            jdbc.closeDataBase();
            connections.remove(connection);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Connection was closed ");
    }

    public void connectionException(Connection connection, Exception ex) {
        connectionClosed(connection);
        ex.printStackTrace();

    }

    public void recievedContent(Message message, Connection connection) {
        String str = message.getContent();
        JSONParser jsonParser = new JSONParser();

        try {
            String query = "";
            Object obj = jsonParser.parse(str);
            JSONObject command = (JSONObject) obj;
            String cmd = command.get("command").toString();
            String word = command.get("param").toString();
            if (cmd.equals("insert")) {
                String otherWord = command.get("param2").toString();
                query = cmd + " " + "into" + " " + "words (word, description) values " + "('" + word + "', '" + otherWord + "');";
            } else if (cmd.equals("delete"))
                query = cmd + " from words where word='" + word + "'";
            else if (cmd.equals("update")) {
                String description = command.get("param2").toString();
                query = cmd + " words set description = '"+ description + "' where word = '" + word + "';";
            }
            if (!query.equals("")) {
                JSONObject jsonObject = jdbc.executeQuery(query);
                if (jsonObject.get("result").equals("success")) {
                    Message mes = new MessageImpl(jsonObject.toJSONString());
                    connection.send(mes);
                }
            }
            if (cmd.equals("select")) {
                query = word;
                JSONObject jsOb = jdbc.readDataBase(query);
                if (jsOb != null) {
                    Message mes = new MessageImpl(jsOb.toJSONString());
                    connection.send(mes);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.startServer();
    }

    private Set<Connection> connections;
    private ServerSocket serverSocket;
    JDBC jdbc = new JDBC();

}
