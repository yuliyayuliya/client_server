package com.julia.task;

import org.json.simple.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC {


    public void createDataBase() throws SQLException, ClassNotFoundException {
        conn = null;
        Class.forName("org.sqlite.JDBC");
        String url = "jdbc:sqlite:tests.db";
        String sql = "CREATE TABLE IF NOT EXISTS words (\n"
                + "	word text ,\n"
                + "	description text\n"
                + ");";

        conn = DriverManager.getConnection(url);
        statmt = conn.createStatement();
        statmt.execute(sql);
        statmt.execute("insert into words (word, description) values ('яблоко', 'фрукт');");
        statmt.execute("insert into words (word, description) values ('вишня', 'ягода');");

    }

    public JSONObject executeQuery(String string) {
        try {
            statmt = conn.createStatement();
            System.out.println(string);
            statmt.execute(string);
            JSONObject result = new JSONObject();
            result.put("result", "success");
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public JSONObject readDataBase(String string) throws SQLException {
        statmt = conn.createStatement();
        String str = "SELECT word, description FROM 'words' WHERE word='" + string + "'";
        System.out.println(str);
        resSet = statmt.executeQuery(str);

        String word = resSet.getString("word");
        String description = resSet.getString("description");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("word", word);
        jsonObject.put("description", description);
        return jsonObject;

    }

    public void closeDataBase() throws SQLException {
        statmt.close();
        resSet.close();
        conn.close();

        System.out.println("Соединения закрыты");
    }

    public static Connection conn;
    public static Statement statmt;
    public static ResultSet resSet;

}

