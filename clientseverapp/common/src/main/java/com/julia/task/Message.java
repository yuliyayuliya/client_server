package com.julia.task;

import java.io.Serializable;

public interface Message extends Serializable {

    public int CLOSE_TYPE = 0;

    public int CONTENT_TYPE = 1;

    String getName();

    String getContent();

    int getType();
}
