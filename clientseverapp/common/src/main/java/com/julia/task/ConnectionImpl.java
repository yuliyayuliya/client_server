package com.julia.task;

import java.io.*;
import java.net.Socket;

public class ConnectionImpl implements Connection, Runnable {


    public ConnectionImpl(Socket socket, ConnectionListener connectionListener) {
        try {
            this.socket = socket;
            this.connectionListener = connectionListener;
            connectionListener.connectionCreated(this);
            outputStream=socket.getOutputStream();
            in= socket.getInputStream();
            Thread t = new Thread(this);
            t.setPriority(Thread.MIN_PRIORITY);
            t.start();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void send(Message message) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void close() {
        needToRun = false;
    }

    public void run() {
        while (needToRun) {
            try {
                int data = in.available();
                if (data != 0) {
                    ObjectInputStream inputStream = new ObjectInputStream(in);
                    Message message = (Message) inputStream.readObject();
                    connectionListener.recievedContent(message,this);
                }else {
                    Thread.sleep(100);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    private Socket socket;
    private ConnectionListener connectionListener;
    private boolean needToRun = true;
    private OutputStream outputStream;
    private InputStream in;
}
