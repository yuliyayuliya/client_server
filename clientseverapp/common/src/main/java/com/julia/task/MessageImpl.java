package com.julia.task;

public class MessageImpl implements Message {

    public MessageImpl(String str) {
        this.content = str;
    }

    public MessageImpl(String name, String content, int type) {
        this.name = name;
        this.content = content;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public int getType() {
        return type;
    }


    @Override
    public String toString() {
        return "MessageImpl{" +
                "name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", type=" + type +
                '}';
    }

    private String name;
    private String content;
    private int type;
}
