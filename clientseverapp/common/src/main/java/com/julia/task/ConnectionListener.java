package com.julia.task;

public interface ConnectionListener {

    void connectionCreated(Connection connection);

    void connectionClosed(Connection connection);

    void connectionException(Connection connection, Exception ex);

    void recievedContent(Message message,Connection connection);
}
