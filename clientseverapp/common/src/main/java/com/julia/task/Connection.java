package com.julia.task;

public interface Connection {

    int PORT = 3333;

    void send(Message message);

    void close();
}
